/**
 * An ordered collection of items, where items are added to the rear and removed
 * from the front.
 */
public class Queue<E> implements QueueADT<E> {

	// TODO
	// You may use a naive expandable circular array or a chain of listnodes.
	// You may NOT use Java's predefined classes such as ArrayList or
	// LinkedList.
	// Circular Array Implementation
	private final int DEFAULT_CAPACITY = 100;
	private int numItems;
	private int rear;
	private int front;
	private E[] queue;

	@SuppressWarnings("unchecked")
	public Queue() {
		front = 0;
		rear = 0;
		numItems = 0;
		queue = (E[]) (new Object[DEFAULT_CAPACITY]);
	}

	/**
	 * Adds an item to the rear of the queue.
	 * 
	 * @param item
	 *            the item to add to the queue.
	 * @throws IllegalArgumentException
	 *             if item is null.
	 */
	public void enqueue(E item) {
		if (item == null) {
			throw new IllegalArgumentException();
		}
		if (numItems == queue.length) {
			expandCapacity();
		}
		queue[rear] = item;
		rear = (rear + 1) % queue.length;
		numItems++;
	}

	/**
	 * Removes an item from the front of the Queue and returns it.
	 * 
	 * @return the front item in the queue.
	 * @throws EmptyQueueException
	 *             if the queue is empty.
	 */
	public E dequeue() {
		if (isEmpty()) {
			throw new EmptyQueueException();
		}

		E result = queue[front];
		front = (front + 1) % queue.length;
		numItems--;
		return result;
	}

	/**
	 * Returns the item at front of the Queue without removing it.
	 * 
	 * @return the front item in the queue.
	 * @throws EmptyQueueException
	 *             if the queue is empty.
	 */
	public E peek() {
		if (isEmpty()) {
			throw new EmptyQueueException();
		}
		return queue[front];
	}

	/**
	 * Returns true iff the Queue is empty.
	 * 
	 * @return true if queue is empty; otherwise false.
	 */
	public boolean isEmpty() {
		return (numItems == 0);
	}

	/**
	 * Removes all items in the queue leaving an empty queue.
	 */
	public void clear() {
		dequeue();
		}

	/**
	 * Returns the number of items in the Queue.
	 * 
	 * @return the size of the queue.
	 */
	public int size() {
		return numItems;
	}

	private void expandCapacity() {
		// expanding should be done using the naive copy-all-elements approach
		if (queue.length == numItems) {
			E[] temp = (E[]) (new Object[queue.length * 2]);
			System.arraycopy(queue, front, temp, front, queue.length - front);
			if (front != 0) {
				System.arraycopy(queue, 0, temp, queue.length, front);
			}
			queue = temp;
			rear = front + numItems - 1;
		}
	}
}