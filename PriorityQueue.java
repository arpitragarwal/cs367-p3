import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * PriorityQueue implemented as a Binary Heap backed by an array. Implements
 * QueueADT. Each entry in the PriorityQueue is of type PriorityQueueNode<E>.
 * First element is array[1]
 * 
 * @author CS367
 *
 * @param <E>
 */
public class PriorityQueue<E> implements QueueADT<PriorityQueueItem<E>> {
	private final int DEFAULT_CAPACITY = 100;

	// Number of elements in heap
	private int currentSize;

	/**
	 * The heap array. First element is array[1]
	 */
	private PriorityQueueItem<E>[] array;

	/**
	 * Construct an empty PriorityQueue.
	 */
	public PriorityQueue() {
		currentSize = 0;
		// the below code initializes the array.. similar code used for
		// expanding.
		array = (PriorityQueueItem<E>[]) Array.newInstance(PriorityQueueItem.class, DEFAULT_CAPACITY + 1);
	}

	/**
	 * Copy constructor
	 * 
	 * @param pq
	 *            PriorityQueue object to be copied
	 */
	public PriorityQueue(PriorityQueue<E> pq) {
		this.currentSize = pq.currentSize;
		this.array = Arrays.copyOf(pq.array, currentSize + 1);
	}

	/**
	 * Adds an item to this PriorityQueue. If array is full, double the array
	 * size.
	 * 
	 * @param item
	 *            object of type PriorityQueueItem<E>.
	 */
	@Override
	public void enqueue(PriorityQueueItem<E> item) {
		// TODO write appropriate code
		// Check if array is full - double if necessary
		if(item==null)
			throw new NullPointerException();

		if (currentSize+1 > DEFAULT_CAPACITY) {
			doubleArray();
		}

		// Check all nodes and find if one with equal priority exists.
		// Add to the existing node's list if it does
		Iterator<PriorityQueueItem<E>> iter = iterator() ;
		PriorityQueueItem<E> curr = null;
		boolean priorityExists = false;
		while(iter.hasNext()){
			curr = iter.next();
			if(curr.compareTo(item)==0){
				priorityExists = true;
				while(!item.getList().isEmpty())
					curr.add(item.getList().dequeue());
			}
		}
		// Else create new node with value added to list and percolate it up
		if(!priorityExists) {
			currentSize++;
			array[currentSize] = item;
			//percolateUp();
			buildHeap();
		}
	}
	
	private void percolateUp(){
		int child_index = currentSize;
		boolean is_done = false;
		while(!is_done){
			int parent_index = child_index/2;
			if(parent_index==0){
				is_done = true;
			} else if (array[child_index].compareTo(array[parent_index])<=0){
				is_done = true;
			} else {
				PriorityQueueItem<E> tmp = array[child_index];
				array[child_index] = array[parent_index];
				array[parent_index] = tmp;
			}
		}

	}

	public int size() {
		// TODO write appropriate code
		return currentSize;
	}

	/**
	 * Returns a PriorityQueueIterator. The iterator should return the
	 * PriorityQueueItems in order of decreasing priority.
	 * 
	 * @return iterator over the elements in this PriorityQueue
	 */
	public Iterator<PriorityQueueItem<E>> iterator() {
		// TODO write appropriate code - see PriorityQueueIterator constructor
		return new PriorityQueueIterator<E>(this);
	}

	/**
	 * Returns the largest item in the priority queue.
	 * 
	 * @return the largest priority item.
	 * @throws NoSuchElementException
	 *             if empty.
	 */
	@Override
	public PriorityQueueItem<E> peek() {
		// TODO fill in appropriate code, replace default return statement
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		return array[1];
	}

	/**
	 * Removes and returns the largest item in the priority queue. Switch last
	 * element with removed element, and percolate down.
	 * 
	 * @return the largest item.
	 * @throws NoSuchElementException
	 *             if empty.
	 */
	@Override
	public PriorityQueueItem<E> dequeue() {
		// TODO
		// Remove first element
		PriorityQueueItem<E> LargestItem = peek();
		array[1] = array[currentSize--];
		// Replace with last element, percolate down
		percolateDown(1);
		return LargestItem;
	}

	/**
	 * Heapify Establish heap order property from an arbitrary arrangement of
	 * items. ie, initial array that does not satisfy heap property. Runs in
	 * linear time.
	 */
	private void buildHeap() {
		for (int i = currentSize / 2; i > 0; i--)
			percolateDown(i);
	}

	/**
	 * Make this PriorityQueue empty.
	 */
	public void clear() {
		// TODO write appropriate code
		currentSize = 0;
	}

	/**
	 * Internal method to percolate down in the heap. <a
	 * href="https://en.wikipedia.org/wiki/Binary_heap#Extract">Wiki</a>}
	 * 
	 * @param hole
	 *            the index at which the percolate begins.
	 */	
	private void percolateDown(int hole) { // remove element from heap
		// TODO
		int parent = hole;
		int left_child = 2*parent;
		int right_child = 2*parent + 1;

		int larger_child = 0;
		while(left_child<=currentSize){
			larger_child = left_child;
			
			if(right_child>currentSize){
				larger_child = left_child;
			}else if(array[left_child].compareTo(array[right_child]) <=0){
				larger_child = right_child;
			}
			
			// swap if parent is smaller than child
			if(array[parent].compareTo(array[larger_child])<0){
				PriorityQueueItem tmp = array[parent];
				array[parent] = array[larger_child];
				array[larger_child] = tmp;
				
				parent = larger_child;
			} else {
				break;
			}
			left_child = 2*parent;
			right_child = left_child + 1;
		}
	}

	/**
	 * Internal method to expand array.
	 */
	private void doubleArray() {
		PriorityQueueItem<E>[] newArray;

		newArray = (PriorityQueueItem<E>[]) Array.newInstance(PriorityQueueItem.class, array.length * 2);

		for (int i = 0; i < array.length; i++)
			newArray[i] = array[i];
		array = newArray;
	}

	@Override
	public boolean isEmpty() {
		if (currentSize == 0)
			return true;
		return false;
	}
}